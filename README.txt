Maxi Belino - 25/Junio/2020
******************************************************************************************
*** Trivia

    Web donde un Usuario (que se registra) puede contestar preguntas de diferentes categorías.
    Cuando contesta bien la pregunta de una categoría esa categoría ya no se le presenta de nuevo
    para contestar.

    Modificamos el proyecto original:

        Usando BluePrints --> reordené los archivos por funcionalidad
        Usando Bootstrap --> para que se vea mejor en la web

******************************************************************************************

*** Base de datos:

    Postgresql, la cual es accedida mediante SQL Alchemy
    ** Configurar el acceso a la Base en el archivo: app > __init__.py

        Ejemplo:

            POSTGRES = {
            'user': 'postgres',
            'pw': 'triviapass',
            'db': 'trivia',
            'host': 'localhost',
            'port': '5432',
        }

    Admin: Interfaz gráfica para ver tablas de la base de datos.

        Acceso al Admin: http://127.0.0.1:5000/admin
        Un usuario con rol de 'admin' puede acceder al Admin para ver/cambiar/agregar datos de las tablas.
        Modelos (Clases) definidas (= Tablas en la base de datos):

            auth >
                Usuario
                Role
            public >
                Categoria
                Pregunta
                Respuesta
                Tiempos

    ROLES y PERMISOS
    ********************

    Definición de 3 Nuevas Clases para hacer Roles que nos permitan
    proteger las rutas (los recursos hechos en routes.py)

    Concepto: Con un permiso protejo rutas

    Clases a definir:

    1. Permission - debemos crear permisos que tengan Necesidades (roles) --> los Permisos tienen Roles
    2. Need - o Necesidad, será una 'Named tupla'; en una namedtupla accedo a un objeto de esta con punto .
        Podemos definir nuestras propias Needs, o bien podemos usar las predefinidas en Flask-Principal.
        Tendremos 2 namedtupla: - RoleNeed
                                - UserNeed

    3. Identity - Identidad asociada al Usuario, y conjunto de Roles que ese Usuario tiene.
    Permissions , Identity --> tienen Roles (Needs)

    IdentityContext: se usa para comparar las necesidades Identity con el Permission

        Ejemplos:
            1: Creación de un Permiso que para ser satisfecho requiere el rol de 'admin'

                admin_permission = Permission(RoleNeed('admin'))

            2: Permiso con un conjunto de Needs:

                root_admin_permission = Permission(RoleNeed('admin'), UserNeed('root'))

    En nuestra TRIVIA:
    ** Role:
        1. Agregaremos un modelo llamado Role, que define los posibles roles que puede tener un usuario.
        2. Definiremos una relación entre el modelo User y el modelo Role
        3. Un usuario puede tener 'n' roles, mientras que un rol pertenece a un único usuario
        4. En el modelo Role pondremos una Foreign Key al modelo de Usuario

    ** Identidad:
        1. Crear una clase Identidad
        2. Identity será una instancia de esta clase, esta instancia identifica a un usuario
        3. Esta instancia se creará inmediatamente después de iniciar sesión a través del
        4. constructor Identity, parametrizado con el id único de usuario (PK del modelo Usuario).
        5. Una Identity posee una lista de necesidades (Needs) que un usuario puede proporcionar,
        mantenidas como una property.
            Para agregar una necesidad a una identidad:
                identity.provides.add(RoleNeed('admin'))
                identity.provides.add(UserNeed('root'))

            Al Identity se agregan todas las necesidades que un usuario puede usar.

    Asegurando una ruta
    *******************
    Crearemos un 1) Permission y una 2) Identity.

    Cada vez que se loguee un usuario se debe asociar a la Identity:
        a) el Usuario (current_user de Flask-Login)
        b) la lista de Roles de ése usuario.

    1) Crear un Permission con rol Admin

        # Creamos un permiso, para ser satisfecho hay que ser Admin
        admin_permission = Permission(RoleNeed('admin'))

    2) Asociaciones: Identity – Usuario actual + Identity – Roles del usuario
        en routes.py, ver la parte de flask-principal

        @identity_loaded.connect_via(app)

            def on_identity_loaded(sender, identity):

                # Seteamos la identidad con el usuario actual
                identity.user = current_user

                # La identidad proveerá una Need para current_user.
                if hasattr(current_user, 'id'):
                    identity.provides.add(UserNeed(current_user.id))

                # Agregamos a la identidad la lista de roles que posee el usuario
                if hasattr(current_user, 'roles'):
                    for role in current_user.roles:
                        identity.provides.add(RoleNeed(role.rolename))

********************************
Algo sobre postgresql:

    1. Cambiar de usuario:
        su postgres

    2. Conectarse al postgre:
        psql

    3. Seleccionar la base de datos:
        postgres-# \c trivia

    4. Listar las tablas:
        trivia=# \dt

    5. Ejecutar una consulta sobre una tabla;
        trivia=# SELECT * FROM usuario;
        trivia=# SELECT * FROM role;


*** Modificar la base de datos:

    Flask-Migrate
    *************
    crea un script para ejecutar sobre la BD y modificar su estructura en base a los
    cambios realizados en los modelos, sin necesidad de escribir Sql.

    Instarlo en el ambiente virtual:
        pip3 install flask-migrate

        Luego recordar actualizar el archivo requirements.txt:
          pip3 freeze > requirements.txt

    Comandos principales de Flask Migrate (desde consola):
        – flask db init: Crea una estructura de directorios y archivos
        necesarios para la ejecución de esta extensión. Se ejecuta solo una
        vez, al principio.
        – flask db migrate: Navega entre los modelos en busca de
        actualizaciones y genera los ficheros de migración de base de datos
        con los cambios detectados.
        – flask db upgrade: Lleva a cabo la migración de la base de datos.


*** Test usando Bootstrap:

    La ruta de prueba: /test_template (definida en public/routes.py)

        En el navegador: http://127.0.0.1:5000/test_template

        La ruta renderiza test_template.html
        y
        test_template.html extiende de base_alternativa.html:
