from flask_wtf import FlaskForm
# StringField, PasswordField, BooleanField, SubmitField
# son 4 clases incluidas en flask-wtf que representan los tipos de datos
# que se esperan en cada campo del formulario:
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired, Length, Email, NoneOf # Use NoneOf en vez de AnyOf


class LoginForm(FlaskForm):
    #  Los atributos de la clase representan los campos que lleva el formulario y se debe especificar el tipo de datos
    email = StringField('Email', validators=[InputRequired(), Email(message="Debe enviar un email")])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=5, max=10), NoneOf({"secret","password"}, message="No es un pass admitido")])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')