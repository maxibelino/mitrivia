from flask_wtf import FlaskForm
# StringField, PasswordField, BooleanField, SubmitField
# son 4 clases incluidas en flask-wtf que representan los tipos de datos
# que se esperan en cada campo del formulario:
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import InputRequired, Length, Email, NoneOf, ValidationError  # Use NoneOf en vez de AnyOf


class RegisterForm(FlaskForm):
    #  Los atributos de la clase representan los campos que lleva el formulario y se debe especificar el tipo de datos
    name = StringField('Name', validators=[InputRequired(), Length(min=2,max=32)])
    email = StringField('Email', validators=[InputRequired(), Email(message="Debe enviar un email")])
    password = PasswordField('Password', validators=[InputRequired(), Length(min=5, max=10), NoneOf({"secret","password"}, message="No es un pass admitido")])
    is_admin = BooleanField("Administrator")
    submit = SubmitField('Sign Up')

    # Lo usé como opcion 1 para el formulario registro
    # Luego me quede con la opcion 2, metodo en la clase Usuario
    #def existe_email(form, correo):
    #    usuarios = Usuario.query.all()
    #    for u in usuarios:
    #        if correo in u.email:
    #            print("ya existe el correo: {}".format(correo))
    #            #raise ValidationError('Ya existe el correo en la base')
    #            return True
    #    return False