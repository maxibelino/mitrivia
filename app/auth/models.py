from app import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


# Para el manejo de Usuario creamos esta clase
# Herencia múltiple, hereda de 2 clases:
class Usuario(db.Model, UserMixin):
    # la palabra user, es una palabra reservada en postgres por eso se pone otro nombre
    # acá para q no haya inconvenientes
    # __tablename__ = 'trivia_user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(256), unique=True, index=True, nullable=False)
    is_admin = db.Column(db.String(64), default=False)
    password_hash = db.Column(db.String(128), nullable=False)
    tiempos = db.relationship('Tiempos', backref='usuario', lazy='dynamic') # Mejor tiempo de juego del usuario
    roles =  db.relationship('Role', backref='usuario', lazy='dynamic')     # Roles del Usuario

    def __repr__(self):
        return f'<User {self.email}>'

    def set_pass(self, password):
        self.password_hash = generate_password_hash(password)

    def check_pass(self, password):
        return check_password_hash(self.password_hash, password)

    def save(self):
        if not self.id:
            db.session.add(self)
            if self.is_admin:
                r = Role(rolename='admin', usuario=self)
                db.session.add(r)
        db.session.commit()

    def actualizar_tiempo(self, segs):  # Actualizo el mejor tiempo del usuario
        for t in self.tiempos:          # en la tabla de Tiempos
            t.tiempo_juego = segs
        db.session.commit()

    # 2 métodos estáticos: son métodos de la clase, no del objeto en particular
    @staticmethod
    def get_by_id(id):
        return Usuario.query.get(id)

    @staticmethod
    def get_by_email(email):
        return Usuario.query.filter_by(email=email).first()



# Clase donde defino los roles (ver Readme.txt)
class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rolename = db.Column(db.String(60), nullable=False)
    usuario_id = db.Column(db.Integer, db.ForeignKey('usuario.id'))
