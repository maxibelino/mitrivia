# app/auth/routes.py

from . import auth_bp
from app import login_manager
from flask import render_template, redirect, url_for, flash, request, session, current_app
from flask_principal import Principal, Permission, Identity, AnonymousIdentity, RoleNeed, UserNeed, identity_loaded, identity_changed

from .models import Usuario
from .forms.login import LoginForm
from .forms.register import RegisterForm

from flask_login import LoginManager, current_user, login_user, login_required, logout_user


# le decimos a Flask-Login como obtener un usuario
@login_manager.user_loader
def load_user(user_id):
    return Usuario.get_by_id(int(user_id))


@auth_bp.route('/trivia/login', methods=['GET', 'POST'])
def login():
    """
    Formulario de Login de Usuarios
    Ruta que atiende el formulario de 2 formas:
    La ruta acepta GET (cuando muestra el form) y POST (cuando procesa los datos)
    """
    if current_user.is_authenticated:
        return redirect(url_for('public.index'))
    # print(request.method)
    # Instanciamos la clase que se encuentra en forms/login.py
    form = LoginForm()
    #
    # form.validate_on_submit()
    #   realiza el trabajo de validación, sólo si hay datos para validar y sólo cuando se envía un POST.
    #   Ese POST lo hace el mismo formulario, tiene action="" porque la ruta será la misma.
    #   En caso de que se llegue a esta ruta vía GET, devuelve false.
    if form.validate_on_submit():
        #
        # La función flash() de Flask permite almacenar mensajes para mostrar en el template html.
        #   Luego obtenemos los mensajes en el template con: get_flashed_messages()
        #   Una vez que obtuvimos los mensajes a través del método get_flashed_messages()
        #   desaparecen de la lista de mensajes
        flash('Login requested for email {}'.format(form.email.data))
        # Verifico los datos ingresados contra la base de datos
        u = Usuario.get_by_email(form.email.data)
        if u is not None and u.check_pass(form.password.data):
            # Función provista por Flask-Login, el segundo parámetro gestión el "recordar"
            # Guardar la info del usuario en la sesión usando Flask-Login
            login_user(u, remember=form.remember_me.data)
            # Tell Flask-Principal the identity changed
            identity_changed.send(current_app._get_current_object(), identity=Identity(u.id))
            # Si el usuario quiso acceder a una pagina que necesita login y llegó hasta acá
            # luego de un login OK lo vamos a redirigir a la página a la q quiso acceder
            next_page = request.args.get('next', None)
            if not next_page:
                next_page = url_for('public.index')
            return redirect(next_page)
        else:
            flash('Usuario o Pass incorrecto')
            return redirect(url_for('auth.login'))
    else:
        print("Fallo en la validación o el método fue GET")
    # Entra aca la 1era vez o si no valida el form.
    # esta variable form se podría haber llamado distinto pero tengo que cambiar tambien
    # el nombre en el template login.html
    return render_template('login.html', form=form)


@auth_bp.route("/trivia/register", methods=["GET", "POST"])
def register():
    """
    Formulario de Registro de Usuarios
    Ruta que atiende el formulario de 2 formas:
    La ruta acepta GET (cuando muestra el form) y POST (cuando procesa los datos)
    """
    print(request.method)
    if current_user.is_authenticated:
        return redirect(url_for('public.index'))
    # Instanciamos la clase que se encuentra en forms/register.py
    form = RegisterForm()
    if form.validate_on_submit():
        # opción 1: Usando un metodo de la clase RegisterForm (en register.py)
        # Verifica si ya existe en la base de datos (porque este campo está definido como unique)
        # if form.existe_email(form.email.data):
        #    flash('El correo ya existe = {}'.format(form.email.data))
        #    return redirect('/trivia/register')
        # opción 2: Usar el método de get_by_email de la clase Usuario
        user = Usuario.get_by_email(form.email.data)
        flash('Register requested for: Name={}, Email={}, Admin={}'.format(form.name.data, form.email.data, form.is_admin.data))
        if user is not None:
            flash('El email {} ya está siendo utilizado por otro usuario'.format(form.email.data))
        else:
            # Creamos el usuario y lo guardamos
            u = Usuario(name=form.name.data, email=form.email.data, is_admin=form.is_admin.data)
            u.set_pass(form.password.data)
            u.save()
            # Dejamos al usuario logueado
            login_user(u, remember=True)
            # Avisar a flask-principal del cambio de estado del usuario
            identity_changed.send(current_app._get_current_object(), identity=Identity(u.id))
            # Luego del registro el usuario llegará a la página inicial del juego
            return redirect(url_for('public.index'))
        return render_template('register.html', form=form)
    else:
        print("Fallo en la validacion de los campos ó el método fue GET")
    return render_template('register.html', form=form)


@auth_bp.route('/trivia/logout')
def logout():
    logout_user()       # es una función que brinda flask-login
    # Flask-Principal: Remove session keys
    for key in ('identity.name', 'identity.auth_type'):
        session.pop(key, None)
    # Tell Flask-Principal the identity changed: the user is now anonymous
    identity_changed.send(current_app._get_current_object(), identity=AnonymousIdentity())
    return redirect(url_for('public.index'))
