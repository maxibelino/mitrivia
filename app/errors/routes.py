# app/errors/routes.py

from . import errors_bp
from flask import render_template, jsonify
from werkzeug.exceptions import HTTPException


@errors_bp.errorhandler(404)
def page_not_found(e):
    # return jsonify(error=str(e)), 404
    return render_template('404.html')


@errors_bp.errorhandler(401)
def unathorized(e):
    return jsonify(error=str(e)), 401


@errors_bp.errorhandler(HTTPException)
def handle_exception(e):
    return jsonify(error=str(e)), e.code
