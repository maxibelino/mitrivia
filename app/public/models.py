from app import db


# modelo Categoria
class Categoria(db.Model):
    # defino 2 columnas o campos de la tabla, id y descripcion
    # el campo q es primary key identifica de forma única al objeto de esta clase
    id = db.Column(db.Integer, primary_key=True)  # el primer parámetro es el tipo de dato
    # descripcion es string y tiene indice; y es unico (no queremos 2 categorias que se llamen igual)
    descripcion = db.Column(db.String(64), index=True, unique=True)
    #
    # La siguiente linea es para definir la relacion con el modelo Pregunta
    # Permite navegar desde una categoria hacia todas las preguntas que le pertenecen
    #   backref es el nombre de la relacion
    #   dynamic = la relacion se arma a medida que preciso
    preguntas = db.relationship('Pregunta', backref='categoria', lazy='dynamic')

    # como quiero imprimir los objetos de esta clase, o, 'representación en string del modelo'
    def __repr__(self):
        return '<Categoria: {}>'.format(self.descripcion)


# modelo Pregunta, es una clase que hereda de db.Models, siempre
# este sera el modelo Debil que almacena la clave del modelo Fuerte (Categoria)
# --> cada Pregunta guarda el id de la Categoria a la que pertenece
class Pregunta(db.Model):
    # Campos
    #   defino 2 campos de la tabla
    #   1. id que es la clave primaria que identifica a  cada objeto Pregunta
    id = db.Column(db.Integer, primary_key=True)
    #   2. text que es el texto de la pregunta
    text = db.Column(db.String(255), nullable=False, unique=True)
    #
    # Forma de relacionarse con la categoría
    #   - una pregunta pertenece solo a una categoría y
    #   - una categoría puede tener muchas preguntas
    # es una relación 1 a n:
    #   1 Pregunta ---> 1 Categoría || 1 Categoría ---> n preguntas
    # Guardar en el modelo débil la clave de una Fuerte
    #   guardo el id de la categoría a la que pertenece esta Pregunta
    categoria_id = db.Column(db.Integer, db.ForeignKey('categoria.id'))
    #
    # Para el ejercicio propuesto:
    # defino una relation para que desde una Pregunta pueda recorrer las posibles respuestas
    respuestas = db.relationship('Respuesta', backref='pregunta', lazy='dynamic')

    def __repr__(self):
        return '<Pregunta %s>' % self.text


class Respuesta(db.Model):
    # campo id es el Primary Key de la tabla
    id = db.Column(db.Integer, primary_key=True)
    # Texto que tiene la respuesta
    text = db.Column(db.String(255), nullable=False, unique=True)
    # ¿Es la respuesta correcta a la Pregunta? Lo defino en este campo booleano,
    # que no sea Nulo, y que x defecto sea False
    correcta = db.Column(db.Boolean, nullable=False, default=False)
    # Esta respuesta corresponde a una pregunta exacta, guardo el id de esa pregunta
    pregunta_id = db.Column(db.Integer, db.ForeignKey('pregunta.id'))

    def __repr__(self):
        return '<Respuesta %s>' % self.text


# Con este modelo registraremos los mejores tiempos de juego de cada usuario
# Cada usuario tiene solo 1 tiempo registrado
class Tiempos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    tiempo_juego = db.Column(db.String(20))
    # Opcional: agregar fecha y hora de la jugada
    # Este tiempo registrado corresponde a un Usuario
    usuario_id = db.Column(db.Integer, db.ForeignKey('usuario.id'))

    def save(self):
        if not self.id:
            db.session.add(self)
        db.session.commit()
