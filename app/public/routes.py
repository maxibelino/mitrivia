from werkzeug.utils import redirect

from app import login_required, current_user
from flask import render_template, session, url_for, flash, request
from . import public_bp
from .models import Categoria, Pregunta, Respuesta, Tiempos
import random
import datetime

from ..auth.models import Usuario


@public_bp.route('/trivia/')
@public_bp.route('/trivia')
def index():
    if not current_user.is_authenticated:
        session.clear()
    return render_template('bienvenidos.html')


@public_bp.route('/trivia/categorias', methods=['GET'])
@login_required
def mostrarcategorias():
    """
    Cuando se llega a esta ruta, comienza el juego
    Para recuperar datos, las instrucciones a ejecutar tienen ese formato:
      – <Modelo>.query.<all|filter_by|get>
      Si usamos all o filter_by, devuelven una lista de objetos (en este caso de esa clase)
    """
    # Categoria es el modelo o clase (import), categorías es una lista de objetos Categoria
    categorias = Categoria.query.all()
    # Si la sesión aun no empezó o el referer es http://server_IP/trivia
    print("Request es: {0} Referer es: {1}".format(request.url, request.referrer))
    if "tiempo_inicio" not in session.keys():
        session['tiempo_inicio'] = datetime.datetime.now()  # guardo el tiempo de inicio del juego
        # Para cada categoría guardo en la sesión un 0 que indica que no fue respondida aún
        for c in categorias:
            session[str(c.id)] = "0"  # ej: session["2"] = "0"
    else:
        # El juego ya está empezado
        # Si viene desde la URL del Home de la Trivia
        # --> asumo que el jugador desea iniciar un nuevo juego (dejó el anterior sin terminar...)
        ref = request.referrer
        if ref == "http://127.0.0.1:5000/trivia" or ref == "http://127.0.0.1:5000/trivia/" or ref is None:
            session['tiempo_inicio'] = datetime.datetime.now()
            for c in categorias:
                session[str(c.id)] = "0"
        # Si hay categorías respondidas OK, esas no las mostraremos en la página
        # sólo las que faltan por contestar OK
        for cat in categorias:  # recorro las categorías
            # Si es distinto de "0" es que ya se contestó correctamente la categoría
            if session[str(cat.id)] != "0":
                categorias.remove(cat)              # --> elimino el item de la lista a mostrar
                # print(cat.id, cat.descripcion)    # Categoria que no se mostrará pues ya se contestó OK
    return render_template('categorias.html',
                           categorias=categorias)  # se llama al template categorias.html y se le pasa la lista que obtuvimos


@public_bp.route('/trivia/<int:id_categoria>/pregunta', methods=['GET'])
@login_required
def mostrarpregunta(id_categoria):
    """
    La siguiente ruta recibe como 'path param' el id de la categoría
    con ese id, trae todas las preguntas que existan para esa categoría
    """
    ref = request.referrer
    if ref == "http://127.0.0.1:5000/trivia/categorias":
        print("Contestando Categoría: {}, Correctas hasta el momento = {}".format(id_categoria,
                                                                                  int(session[str(id_categoria)])))
        # Del modelo Pregunta, quiero filtrar todas las preguntas q tengan como id la categoría que me llega
        # como parámetro. El nombre categoria_id coincide con el del modelo:
        # categoria_id = db.Column(db.Integer, db.ForeignKey('categoria.id'))
        preguntas = Pregunta.query.filter_by(categoria_id=id_categoria).all()
        # Elegir pregunta aleatoria pero de la categoría adecuada
        pregunta = random.choice(preguntas)
        # Traer un objeto Categoría usando el get con el id recibido
        categ = Categoria.query.get(id_categoria)
        #
        # Dibujar la pagina html usando preguntas.html
        #   Enviandole como datos: el objeto categoria, y el objeto pregunta
        #   hay 2 formas posibles de recorrer/obtener las respuestas:
        #   1. (no la probe)
        #       respuestas_posibles = Respuesta.query.filter_by(pregunta_id=pregunta.id)
        #   2. Para una categoría se trae las preguntas
        respuestas_posibles = pregunta.respuestas
        # for r in respuestas_posibles:
        #        print (r.id, r.text)
        # el primer nombre debe ser el mismo q le llega l template, si pongo: categoria=categ, es porque
        # el template trabajará con el nombre categoria
        return render_template('preguntas.html', categoria=categ, pregunta=pregunta,
                               respuestas_posibles=respuestas_posibles)
    else:
        return redirect(url_for("public.index"))


@public_bp.route('/trivia/<int:id_categoria>/<int:pregunta_id>/respuesta/<int:id_respuesta>', methods=['GET'])
@login_required
def evaluar_respuesta(id_categoria, pregunta_id, id_respuesta):
    """
        Ruta para los links que vienen del template preguntas, donde se arman
        los links que tienen la forma:
        <a href="/trivia/{{categoria.id}}/{{pregunta.id}}/respuesta/{{r.id}}">
    """
    if "tiempo_inicio" not in session.keys():  # En el caso en que el juego terminó y por ej pulsan F5
        flash('Debe iniciar un nuevo juego')
        return redirect(url_for("public.index"))
    r = Respuesta.query.get(id_respuesta)  # Trae un objeto Respuesta con el id
    # print (r.id, r.correcta, r.text, r.pregunta_id, r.pregunta)
    if r.pregunta_id == pregunta_id and r.correcta:  # En la base de datos consulto si r.correcta es True o False
        message = "CORRECTA"
        # Aumento en 1 el contador de la categoría respondida
        session[str(id_categoria)] = int(session[str(id_categoria)]) + 1
        # Si ya no quedan categorías por responder redirige a la pagina ganador.html
        # si no, sigue en respuestas.html
        faltan_x_contestar = False
        categs = Categoria.query.all()  # trae la lista de categorías
        for c in categs:  # recorro la lista
            # si para esa categoría guardé un 0, es que no hay respuestas correctas
            if session[str(c.id)] == "0":
                faltan_x_contestar = True
        # Si ya se contestaron todas las categorías: hallo el tiempo de juego
        if not faltan_x_contestar:
            tjuego = str(datetime.datetime.now() - session['tiempo_inicio'])[5:]
            session['tiempo_total'] = tjuego
            mejor_tiempo = tjuego
            # Guardo el tiempo de juego para el Usuario actual
            u = Usuario.get_by_id(current_user.get_id())
            if u is not None:
                if u.tiempos.count() == 0:  # Si no existe un tiempo registrado para este usuario, lo guardo
                    t = Tiempos(tiempo_juego=tjuego, usuario=u)
                    t.save()
                else:  # Ya existe un registro de tiempo, conservaré el mejor tiempo
                    for t in u.tiempos:
                        mejor_tiempo = tjuego if float(t.tiempo_juego) > float(tjuego) else t.tiempo_juego
                    # Si el mejor tiempo fue el de la jugada actual, actualizo la base
                    if mejor_tiempo == tjuego:
                        print("borrar registro mayor y guardar el nuevo")
                        u.actualizar_tiempo(tjuego)
                    else:
                        print("me quedo con el registro que ya existe en la tabla")
                # Limpio la sesión por si comienza el juego de nuevo
                #       1. Asigno 0s en las categorías
                #       2. Elimino la clave tiempo_inicio
                # 1)
                for c in categs:  # recorro la lista
                    session[str(c.id)] = 0
                # 2)
                session.pop("tiempo_inicio")
            # Traer los 10 mejores tiempos registrados (es decir, los 10 mejores jugadores)
            mejores = Tiempos.query.order_by(Tiempos.tiempo_juego).outerjoin(Usuario,
                                                                             Usuario.id == Tiempos.usuario_id).limit(10)
            # Para ver qué hay en "mejores", ver los sig. comentarios:
            # print(mejores)
            # for n in mejores:
            #    print(n.usuario.name)  # con el id de usuario hice un join desde la tabla Tiempos a la de Usuarios
            return render_template('ganador.html', mejores=mejores)  # esta fue la ultima categoria contestada OK
    else:
        message = "INCORRECTA"
    return render_template('respuestas.html', message=message)


@public_bp.route('/test_template')
def test_principal_free():
    lista_de_cosas = ["item1", "item2", "item3"]
    return render_template('test_template.html', subtitulo="subtitulo", texto="este es el texto", lista=lista_de_cosas,
                           precio="7676.8")
