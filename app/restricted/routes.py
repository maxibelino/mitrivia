# app/restricted/routes.py
from flask import render_template, g, url_for, request, abort
from flask_admin import AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user, login_required
from flask_principal import Permission, RoleNeed, UserNeed, identity_loaded
from werkzeug.utils import redirect

from . import restricted_bp

from app import db, admin, app
from app.auth.models import Usuario, Role
from app.public.models import Categoria, Pregunta, Respuesta, Tiempos


'''
    1. Restringir el acceso a los modelos, dentro del Admin
    Queremos que el manejo del Admin (interfaz gráfica web) sea restringido
    a los usuarios que tienen perfil admin. Para eso, no uso la clase ModelView,
    hago mi clase MyModelView (que extiende de la anterior) para redefinir
    un método de la clase que se llama is_accesible()
    
    2. Restringir el acceso al Home del Admin
    Para esto definí la clase MyAdminIndexView que hereda de AdminIndexView para
    poder redefinir los métodos
'''

# Flask-Principal: Creamos un permiso, para ser satisfecho hay que ser Admin
admin_permission = Permission(RoleNeed('admin'))    # objeto admin_permission, instancia de la clase Permission


# Flask-Principal: Agregamos las necesidades a una identidad, una vez que se loguee el usuario.
@identity_loaded.connect
def on_identity_loaded(sender, identity):
    # Seteamos la identidad al usuario
    identity.user = current_user
    # Agregamos una UserNeed a la identidad, con el usuario actual.
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))
    # Agregamos a la identidad la lista de roles que posee el usuario actual.
    if hasattr(current_user, 'roles'):
        for role in current_user.roles:
            identity.provides.add(RoleNeed(role.rolename))


class MyModelView(ModelView):

    def is_accessible(self):
        # return False                              # Así, el Modelo de Usuario no será visible en el Admin
        # return current_user.is_authenticated      # Así, lo hago visible sólo a los que se loguearon
        has_auth = current_user.is_authenticated    # Así, verifica si está logueado y en la prox linea
        has_perm = admin_permission.allows(g.identity)  # verifica si tiene permiso

        return has_auth and has_perm

    # Redirección: Si en lugar de un 403 queremos redirigir, sobreescribimos el metodo:
    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('auth.login', next=request.url))


# Si también queremos limitar el acceso a la Home del Admin
# debemos hacer nuestra propia vista y cuando se crea la instancia
# del Admin, la pasamos como parámetro
# -----------------------------------------------------------
class MyAdminIndexView(AdminIndexView):

    def is_accessible(self):
        has_auth = current_user.is_authenticated
        has_perm = admin_permission.allows(g.identity)
        return has_auth and has_perm

    # Cuando no esta logueado el usuario lo mandamos a la página de login
    # y le pasamos como parámetro en 'next', la página a la que tiene que ir luego
    # de loguearse; que es a la que el usuario hizo el request inicialmente
    def inaccessible_callback(self, name, **kwargs):
        has_auth = current_user.is_authenticated
        has_perm = admin_permission.allows(g.identity)
        # esta loggeado pero no es admin
        if has_auth and not has_perm:
            abort(401)
        # no loggeado
        else:
            return redirect(url_for('auth.login', next=request.url))

admin.init_app(app, index_view=MyAdminIndexView())

# print("admin:", admin.index_view.name)

# Acá especificamos los modelos que queremos mostrar en el admin
#   en el browser: http://localhost:5000/admin/
admin.add_view(MyModelView(Categoria, db.session))  # ModelView es una clase de Flask Admin
admin.add_view(MyModelView(Pregunta, db.session))
admin.add_view(MyModelView(Respuesta, db.session))
admin.add_view(MyModelView(Usuario, db.session))    # Uso MyModelView para sobreescribir metodos y que el Modelo (clase) Usuario sea restringido
admin.add_view(MyModelView(Tiempos, db.session))
admin.add_view(MyModelView(Role, db.session))       # Agrego la clase/modelo Role para que sea visible en el Admin


@restricted_bp.route('/test')
@admin_permission.require(http_exception=403)
@login_required
def test_principal():
    return render_template('test.html')